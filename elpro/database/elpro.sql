-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 26 Agu 2019 pada 04.11
-- Versi server: 10.1.36-MariaDB
-- Versi PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elpro`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_bank`
--

CREATE TABLE `ms_bank` (
  `id_bank` int(11) NOT NULL,
  `kd_bank` varchar(50) DEFAULT NULL,
  `nm_bank` varchar(50) DEFAULT NULL,
  `namacabang_bank` varchar(50) DEFAULT NULL,
  `norek_bank` varchar(50) DEFAULT NULL,
  `atasnama_bank` varchar(50) DEFAULT NULL,
  `ket_bank` text,
  `is_del_bank` tinyint(4) DEFAULT '0' COMMENT '0 = aktif, 1 = tidak aktif',
  `isaktif_bank` tinyint(4) DEFAULT '0' COMMENT '0 = aktif, 1 = tidak aktif',
  `crdate_bank` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_bank` tinyint(4) DEFAULT NULL,
  `update_bank` timestamp NULL DEFAULT NULL,
  `id_up_bank` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='master bank\r\n';

--
-- Dumping data untuk tabel `ms_bank`
--

INSERT INTO `ms_bank` (`id_bank`, `kd_bank`, `nm_bank`, `namacabang_bank`, `norek_bank`, `atasnama_bank`, `ket_bank`, `is_del_bank`, `isaktif_bank`, `crdate_bank`, `id_cr_bank`, `update_bank`, `id_up_bank`) VALUES
(24, '002', 'BRI', 'Depok', '312313', 'Pak Bos', 'ini rekening pak bos depok ya', 0, 0, '2019-08-19 03:13:11', 1, NULL, NULL),
(25, '003', 'sadf', 'sdafa', 'ssdf', 'asdfa', 'dsfasd', 0, 0, '2019-08-25 06:25:58', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_cabang`
--

CREATE TABLE `ms_cabang` (
  `id_cb` int(11) NOT NULL,
  `kd_cb` varchar(50) DEFAULT NULL,
  `nm_cb` varchar(50) DEFAULT NULL,
  `ket_cb` text,
  `is_del_cb` tinyint(4) DEFAULT '0',
  `crdate_cb` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_cb` tinyint(4) DEFAULT NULL,
  `update_cb` timestamp NULL DEFAULT NULL,
  `id_up_cb` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='master cabang\r\n';

--
-- Dumping data untuk tabel `ms_cabang`
--

INSERT INTO `ms_cabang` (`id_cb`, `kd_cb`, `nm_cb`, `ket_cb`, `is_del_cb`, `crdate_cb`, `id_cr_cb`, `update_cb`, `id_up_cb`) VALUES
(22, 'DP001', 'Depok', 'Cabang Depok', 0, '2019-08-15 04:54:14', 1, NULL, NULL),
(23, 'BGR001', 'Bogor', 'Cabang Bogor', 0, '2019-08-15 04:54:30', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_cara_pembayaran`
--

CREATE TABLE `ms_cara_pembayaran` (
  `id_cp` int(11) NOT NULL,
  `kd_cp` varchar(50) DEFAULT NULL,
  `nm_cp` varchar(50) DEFAULT NULL,
  `ket_cp` text,
  `is_del_cp` tinyint(4) DEFAULT '0',
  `crdate_cp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_cp` tinyint(4) DEFAULT NULL,
  `update_cp` timestamp NULL DEFAULT NULL,
  `id_up_cp` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='master cara pembayaran\r\n';

--
-- Dumping data untuk tabel `ms_cara_pembayaran`
--

INSERT INTO `ms_cara_pembayaran` (`id_cp`, `kd_cp`, `nm_cp`, `ket_cp`, `is_del_cp`, `crdate_cp`, `id_cr_cp`, `update_cp`, `id_up_cp`) VALUES
(13, '001', 'cash keras', 'cash keras', 0, '2019-08-16 03:46:08', 1, NULL, NULL),
(14, '002', 'cash bertahap', 'cash bertahap', 0, '2019-08-16 03:43:16', 1, NULL, NULL),
(15, '003', 'cicil deveoper', 'cicil deveoper', 0, '2019-08-16 03:43:30', 1, NULL, NULL),
(16, '004', 'kpr konvensional', 'kpr konvensional', 0, '2019-08-16 03:43:41', 1, NULL, NULL),
(17, '005', 'kpr syariah', 'kpr syariah', 0, '2019-08-16 03:43:52', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_jenis_properti`
--

CREATE TABLE `ms_jenis_properti` (
  `id_jp` int(11) NOT NULL,
  `kd_jp` varchar(50) DEFAULT NULL,
  `nm_jp` varchar(50) DEFAULT NULL,
  `ket_jp` text,
  `is_del_jp` tinyint(4) DEFAULT '0',
  `crdate_jp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_jp` tinyint(4) DEFAULT NULL,
  `update_jp` timestamp NULL DEFAULT NULL,
  `id_up_jp` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='master jenis properti\r\n';

--
-- Dumping data untuk tabel `ms_jenis_properti`
--

INSERT INTO `ms_jenis_properti` (`id_jp`, `kd_jp`, `nm_jp`, `ket_jp`, `is_del_jp`, `crdate_jp`, `id_cr_jp`, `update_jp`, `id_up_jp`) VALUES
(3, '001', 'rumah', 'rumah', 0, '2019-08-16 03:45:53', 1, NULL, NULL),
(4, '002', 'tanah', 'tanah', 0, '2019-08-16 03:45:55', 1, NULL, NULL),
(5, '003', 'ruko', 'ruko', 0, '2019-08-16 03:45:57', 1, NULL, NULL),
(6, '004', 'apartemen', 'apartemen', 0, '2019-08-16 03:45:59', 1, NULL, NULL),
(7, '005', 'lainnya', 'lainnya', 0, '2019-08-16 03:46:03', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_jenis_transaksi`
--

CREATE TABLE `ms_jenis_transaksi` (
  `id_jt` int(11) NOT NULL,
  `kd_jt` varchar(50) DEFAULT NULL,
  `nm_jt` varchar(50) DEFAULT NULL,
  `ket_jt` text,
  `is_del_jt` tinyint(4) DEFAULT '0',
  `crdate_jt` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_jt` tinyint(4) DEFAULT NULL,
  `update_jt` timestamp NULL DEFAULT NULL,
  `id_up_jt` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='master jenis transaksi\r\n\r\n';

--
-- Dumping data untuk tabel `ms_jenis_transaksi`
--

INSERT INTO `ms_jenis_transaksi` (`id_jt`, `kd_jt`, `nm_jt`, `ket_jt`, `is_del_jt`, `crdate_jt`, `id_cr_jt`, `update_jt`, `id_up_jt`) VALUES
(2, '001', 'jual', 'jual', 0, '2019-08-16 04:00:26', 1, NULL, NULL),
(3, '002', 'primary', 'primary', 0, '2019-08-16 04:00:28', 1, NULL, NULL),
(4, '003', 'sewa', 'sewa', 0, '2019-08-16 04:00:30', 1, NULL, NULL),
(5, '004', 'secondary', 'secondary', 0, '2019-08-16 04:00:32', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_marketing`
--

CREATE TABLE `ms_marketing` (
  `id_mr` int(11) NOT NULL,
  `kd_mr` varchar(50) DEFAULT NULL,
  `nm_mr` varchar(50) DEFAULT NULL,
  `alamat_mr` text,
  `ket_mr` text,
  `is_del_mr` tinyint(4) DEFAULT '0' COMMENT '0 on, 1 off',
  `jk_mr` tinyint(4) DEFAULT '0' COMMENT '0 lk, 1 pr',
  `id_type_mrk` tinyint(4) DEFAULT NULL COMMENT 'id type marketing',
  `id_cb_mrk` tinyint(4) DEFAULT NULL COMMENT 'id cabang marketing',
  `id_tim_mr` tinyint(4) DEFAULT NULL COMMENT 'id tim marketing',
  `is_active_mr` tinyint(4) DEFAULT '0' COMMENT '0 on, 1 off',
  `crdate_mr` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_mr` tinyint(4) DEFAULT NULL,
  `update_mr` timestamp NULL DEFAULT NULL,
  `id_up_mr` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='master marketing\r\n';

--
-- Dumping data untuk tabel `ms_marketing`
--

INSERT INTO `ms_marketing` (`id_mr`, `kd_mr`, `nm_mr`, `alamat_mr`, `ket_mr`, `is_del_mr`, `jk_mr`, `id_type_mrk`, `id_cb_mrk`, `id_tim_mr`, `is_active_mr`, `crdate_mr`, `id_cr_mr`, `update_mr`, `id_up_mr`) VALUES
(5, 'Selling name', 'name selling', 'tes', 'tes', 0, 0, 1, 22, 24, 0, '2019-08-25 10:00:23', 1, NULL, NULL),
(6, 'listingcode', 'listing name', 'sdfa', 'dsfas', 0, 0, 2, 22, 24, 0, '2019-08-25 10:00:37', 1, NULL, NULL),
(7, 'colisting', 'colisting name', 'dsfa', 'sdafasd', 0, 0, 3, 22, 24, 0, '2019-08-25 10:00:52', 1, NULL, NULL),
(8, 'cosellingcode', 'coselling name', 'sdfa', 'sdafa', 0, 0, 4, 22, 24, 0, '2019-08-25 10:01:11', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_tim`
--

CREATE TABLE `ms_tim` (
  `id_tim` int(11) NOT NULL,
  `kd_tim` varchar(50) DEFAULT NULL,
  `nm_tim` varchar(50) DEFAULT NULL,
  `ket_tim` text,
  `is_del_tim` tinyint(4) DEFAULT '0',
  `crdate_tim` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_tim` tinyint(4) DEFAULT NULL,
  `update_tim` timestamp NULL DEFAULT NULL,
  `id_up_tim` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='master tim\r\n';

--
-- Dumping data untuk tabel `ms_tim`
--

INSERT INTO `ms_tim` (`id_tim`, `kd_tim`, `nm_tim`, `ket_tim`, `is_del_tim`, `crdate_tim`, `id_cr_tim`, `update_tim`, `id_up_tim`) VALUES
(24, '0013', 'Ayes 131', 'siiip 1231', 0, '2019-08-20 02:34:59', 1, '2019-08-20 02:34:59', 1),
(25, '0013', 'asdfas', 'siiip 1231', 0, '2019-08-20 02:34:59', 1, '2019-08-20 02:34:59', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_type_marketing`
--

CREATE TABLE `ms_type_marketing` (
  `id_type_mr` int(11) NOT NULL,
  `kd_type_mr` varchar(50) DEFAULT NULL,
  `nm_type_mr` varchar(50) DEFAULT NULL,
  `ket_type_mr` text,
  `is_del_type_mr` tinyint(4) DEFAULT '0',
  `crdate_type_mr` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_type_mr` tinyint(4) DEFAULT NULL,
  `update_type_mr` timestamp NULL DEFAULT NULL,
  `id_up_type_mr` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='master type marketing\r\n\r\n';

--
-- Dumping data untuk tabel `ms_type_marketing`
--

INSERT INTO `ms_type_marketing` (`id_type_mr`, `kd_type_mr`, `nm_type_mr`, `ket_type_mr`, `is_del_type_mr`, `crdate_type_mr`, `id_cr_type_mr`, `update_type_mr`, `id_up_type_mr`) VALUES
(1, '001', 'selling', 'selling', 0, '2019-08-20 03:18:44', 1, '2019-08-20 03:18:44', 1),
(2, '002', 'Listing', 'listing', 0, '2019-08-20 03:17:18', 1, NULL, NULL),
(3, '003', 'co-listing', 'colisting', 0, '2019-08-20 03:18:28', 1, '2019-08-20 03:18:28', 1),
(4, '004', 'co-selling', 'co-selling', 0, '2019-08-20 03:18:17', 1, NULL, NULL),
(5, '005', 'ul peselling', 'ul peselling', 0, '2019-08-20 03:28:54', 1, NULL, NULL),
(6, '006', 'ul pelisting', 'ul pelisting', 0, '2019-08-20 03:29:18', 1, NULL, NULL),
(7, '007', 'ul coselling', 'ul coselling', 0, '2019-08-20 03:31:02', 1, NULL, NULL),
(8, '008', 'ul colisting', 'ul colisting', 0, '2019-08-20 03:31:12', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hak_akses`
--

CREATE TABLE `tbl_hak_akses` (
  `id` int(11) NOT NULL,
  `id_user_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_hak_akses`
--

INSERT INTO `tbl_hak_akses` (`id`, `id_user_level`, `id_menu`) VALUES
(15, 1, 1),
(19, 1, 3),
(21, 2, 1),
(28, 2, 3),
(29, 2, 2),
(30, 1, 2),
(38, 3, 10),
(39, 3, 9),
(41, 1, 11),
(42, 1, 12),
(45, 1, 13),
(46, 1, 14),
(47, 1, 15),
(49, 1, 16),
(50, 1, 17),
(51, 1, 18),
(52, 1, 20),
(53, 1, 19);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `url` varchar(30) NOT NULL,
  `icon` varchar(30) NOT NULL,
  `is_main_menu` int(11) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL COMMENT 'y=yes,n=no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `title`, `url`, `icon`, `is_main_menu`, `is_aktif`) VALUES
(1, 'KELOLA MENU', 'kelolamenu', 'fa fa-server', 0, 'y'),
(2, 'KELOLA PENGGUNA', 'user', 'fa fa-user-o', 0, 'y'),
(3, 'level PENGGUNA', 'userlevel', 'fa fa-users', 0, 'y'),
(11, 'Master', '#', 'fa fa-user', 0, 'y'),
(12, 'Cabang', 'ms_cabang', 'fa fa-user', 11, 'y'),
(13, 'Cara Pembayaran', 'ms_cara_pembayaran', 'fa fa-user', 11, 'y'),
(14, 'Jenis Properti', 'ms_jenis_properti', 'fa fa-user', 11, 'y'),
(15, 'Jenis Transaksi', 'ms_jenis_transaksi', 'fa fa-user', 11, 'y'),
(16, 'Bank', 'ms_bank', 'fa fa-money', 11, 'y'),
(17, 'Marketing', 'ms_marketing', 'fa fa-user', 11, 'y'),
(18, 'Form Komisi', '#', 'fa fa-edit', 0, 'y'),
(19, 'Klaim Komisi Marketing', 'tr_klaim_komisi_marketing', 'fa fa-circle-o', 18, 'y'),
(20, 'Tim', 'ms_tim', 'fa fa-circle-o', 11, 'y'),
(21, 'Type Marketing', 'ms_type_marketing', 'fa fa-circle-o', 11, 'y'),
(22, 'Klaim Komisi Member', 'tr_klaim_komisi_member', 'fa fa-circle-o', 18, 'y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_setting`
--

CREATE TABLE `tbl_setting` (
  `id_setting` int(11) NOT NULL,
  `nama_setting` varchar(50) NOT NULL,
  `value` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_setting`
--

INSERT INTO `tbl_setting` (`id_setting`, `nama_setting`, `value`) VALUES
(1, 'Tampil Menu', 'ya');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_users` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `images` text NOT NULL,
  `id_user_level` int(11) NOT NULL,
  `is_aktif` enum('y','n') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id_users`, `full_name`, `email`, `password`, `images`, `id_user_level`, `is_aktif`) VALUES
(1, 'Superadmin', 'superadmin@gmail.com', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', '21.jpg', 1, 'y'),
(3, 'Admin', 'admin@gmail.com', '$2y$04$Wbyfv4xwihb..POfhxY5Y.jHOJqEFIG3dLfBYwAmnOACpH0EWCCdq', '22.jpg', 2, 'y'),
(7, 'joni', 'joni@gmail.com', '$2y$04$FQ9o5NaMbEmJ9/1ux8NIquPdTu46zU8BWLHZKaHTIW06iOIFQ5t3q', '2.jpg', 3, 'y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user_level`
--

CREATE TABLE `tbl_user_level` (
  `id_user_level` int(11) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user_level`
--

INSERT INTO `tbl_user_level` (`id_user_level`, `nama_level`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Finance');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_detail_cp_kkm`
--

CREATE TABLE `tr_detail_cp_kkm` (
  `id_detail_kkm` int(11) NOT NULL,
  `id_cpkkm_detail` tinyint(4) DEFAULT NULL,
  `id_cp_dkkm` tinyint(4) DEFAULT NULL,
  `is_del_dkkm` tinyint(4) DEFAULT '0' COMMENT '0 = aktif, 1 = tidak aktif',
  `isaktif_dkkm` tinyint(4) DEFAULT '0' COMMENT '0 = aktif, 1 = tidak aktif',
  `crdate_dkkm` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_dkkm` tinyint(4) DEFAULT NULL,
  `update_dkkm` timestamp NULL DEFAULT NULL,
  `id_up_dkkm` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='transaksi detail klaim komisi marketing';

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_detail_jt_kkm`
--

CREATE TABLE `tr_detail_jt_kkm` (
  `id_detail_kkm` int(11) NOT NULL,
  `id_jtkkm_detail` tinyint(4) DEFAULT NULL,
  `id_jt_dkkm` tinyint(4) DEFAULT NULL,
  `is_del_dkkm` tinyint(4) DEFAULT '0' COMMENT '0 = aktif, 1 = tidak aktif',
  `isaktif_dkkm` tinyint(4) DEFAULT '0' COMMENT '0 = aktif, 1 = tidak aktif',
  `crdate_dkkm` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_dkkm` tinyint(4) DEFAULT NULL,
  `update_dkkm` timestamp NULL DEFAULT NULL,
  `id_up_dkkm` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='transaksi detail klaim komisi marketing';

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_detail_kkm`
--

CREATE TABLE `tr_detail_kkm` (
  `id_detail_kkm` int(11) NOT NULL,
  `id_kkm_detail` tinyint(4) DEFAULT NULL,
  `id_jp_dkkm` tinyint(4) DEFAULT NULL,
  `dll_kkm` varchar(50) DEFAULT NULL,
  `id_jt_dkkm` tinyint(4) DEFAULT NULL,
  `id_cp_dkkm` tinyint(4) DEFAULT NULL,
  `is_del_dkkm` tinyint(4) DEFAULT '0' COMMENT '0 = aktif, 1 = tidak aktif',
  `isaktif_dkkm` tinyint(4) DEFAULT '0' COMMENT '0 = aktif, 1 = tidak aktif',
  `crdate_dkkm` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_dkkm` tinyint(4) DEFAULT NULL,
  `update_dkkm` timestamp NULL DEFAULT NULL,
  `id_up_dkkm` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='transaksi detail klaim komisi marketing';

--
-- Dumping data untuk tabel `tr_detail_kkm`
--

INSERT INTO `tr_detail_kkm` (`id_detail_kkm`, `id_kkm_detail`, `id_jp_dkkm`, `dll_kkm`, `id_jt_dkkm`, `id_cp_dkkm`, `is_del_dkkm`, `isaktif_dkkm`, `crdate_dkkm`, `id_cr_dkkm`, `update_dkkm`, `id_up_dkkm`) VALUES
(41, 38, 1, NULL, 1, 1, 0, 0, '2019-08-25 15:41:30', NULL, NULL, NULL),
(42, 38, 2, NULL, 2, 2, 0, 0, '2019-08-25 15:41:30', NULL, NULL, NULL),
(43, 38, 3, NULL, 3, 3, 0, 0, '2019-08-25 15:41:30', NULL, NULL, NULL),
(44, 38, 4, NULL, 4, 4, 0, 0, '2019-08-25 15:41:30', NULL, NULL, NULL),
(45, 38, NULL, NULL, NULL, 5, 0, 0, '2019-08-25 15:41:30', NULL, NULL, NULL),
(46, 39, 1, NULL, 1, 1, 0, 0, '2019-08-25 15:43:01', NULL, NULL, NULL),
(47, 39, 2, NULL, 2, NULL, 0, 0, '2019-08-25 15:43:01', NULL, NULL, NULL),
(48, 39, 4, NULL, NULL, NULL, 0, 0, '2019-08-25 15:43:01', NULL, NULL, NULL),
(49, 40, 5, 'yaheeee', 3, 2, 0, 0, '2019-08-25 15:45:54', NULL, NULL, NULL),
(50, 40, NULL, 'yaheeee', 4, 3, 0, 0, '2019-08-25 15:45:54', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_klaim_komisi_marketing`
--

CREATE TABLE `tr_klaim_komisi_marketing` (
  `id_kkm` int(11) NOT NULL,
  `kd_kkm` varchar(50) DEFAULT NULL,
  `id_jp_kkm` tinyint(4) DEFAULT NULL,
  `id_jt_kkm` tinyint(4) DEFAULT NULL,
  `id_cp_kkm` tinyint(4) DEFAULT NULL,
  `id_mr_kkms` tinyint(4) DEFAULT NULL,
  `id_mr_sel` tinyint(4) DEFAULT NULL,
  `id_mr_cosel` tinyint(4) DEFAULT NULL,
  `id_mr_lis` tinyint(4) DEFAULT NULL,
  `id_mr_colis` tinyint(4) DEFAULT NULL,
  `nm_kkm` varchar(50) DEFAULT NULL,
  `nm_dev` varchar(50) DEFAULT NULL,
  `nm_properti` varchar(50) DEFAULT NULL,
  `nm_cust` varchar(50) DEFAULT NULL,
  `tgl_terjual` date DEFAULT NULL,
  `harga_jual` varchar(50) DEFAULT NULL,
  `komisi` varchar(50) DEFAULT NULL,
  `ket_kkm` text,
  `dll_kkm` text COMMENT 'lainnya',
  `is_del_kkm` tinyint(4) DEFAULT '0',
  `crdate_kkm` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_kkm` tinyint(4) DEFAULT NULL,
  `update_kkm` timestamp NULL DEFAULT NULL,
  `id_up_kkm` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='transaksi klaim komisi marketing';

--
-- Dumping data untuk tabel `tr_klaim_komisi_marketing`
--

INSERT INTO `tr_klaim_komisi_marketing` (`id_kkm`, `kd_kkm`, `id_jp_kkm`, `id_jt_kkm`, `id_cp_kkm`, `id_mr_kkms`, `id_mr_sel`, `id_mr_cosel`, `id_mr_lis`, `id_mr_colis`, `nm_kkm`, `nm_dev`, `nm_properti`, `nm_cust`, `tgl_terjual`, `harga_jual`, `komisi`, `ket_kkm`, `dll_kkm`, `is_del_kkm`, `crdate_kkm`, `id_cr_kkm`, `update_kkm`, `id_up_kkm`) VALUES
(38, 'sadfasdfasd', NULL, NULL, NULL, NULL, 5, 8, 6, 7, NULL, 'sdfasd', 'fsdfas', 'sdfasfa', '2019-08-31', '242342', '23424', 'sdafa', '', 0, '2019-08-25 15:41:30', 1, NULL, NULL),
(39, 'SOPPPP', NULL, NULL, NULL, NULL, 5, 8, 6, 7, NULL, 'dfasf', 'sdfas', 'sdfasd', '2019-08-31', '234242', '2342', 'asdfads', '', 0, '2019-08-25 15:43:01', 1, NULL, NULL),
(40, 'asfdasfasd', NULL, NULL, NULL, NULL, 5, 8, 6, 7, NULL, 'sdfasd', 'sdfas', 'sdfas', '2019-08-31', '2342', '2342', 'dsfas', NULL, 0, '2019-08-25 15:45:54', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tr_klaim_komisi_member`
--

CREATE TABLE `tr_klaim_komisi_member` (
  `id_kk_member` int(11) NOT NULL,
  `kd_kk_member` varchar(50) DEFAULT NULL,
  `nm_properti_member` varchar(50) DEFAULT NULL,
  `nm_mr_sel` varchar(50) DEFAULT NULL,
  `nm_mr_pelisting` varchar(50) DEFAULT NULL,
  `nm_mr_coselling` varchar(50) DEFAULT NULL,
  `nm_mr_colisting` varchar(50) DEFAULT NULL,
  `nm_cust_member` varchar(50) DEFAULT NULL,
  `harga_jual_member` varchar(50) DEFAULT NULL,
  `komisi_awal_member` varchar(50) DEFAULT NULL,
  `komisi_member` varchar(50) DEFAULT NULL,
  `sisa_komisi_member` varchar(50) DEFAULT NULL,
  `nama_member` varchar(50) DEFAULT NULL,
  `no_group_member` varchar(50) DEFAULT NULL,
  `no_rek_bca` varchar(50) DEFAULT NULL,
  `nama_rek_bca` varchar(50) DEFAULT NULL,
  `is_del_member` tinyint(4) DEFAULT '0',
  `crdate_member` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_cr_member` tinyint(4) DEFAULT NULL,
  `update_member` timestamp NULL DEFAULT NULL,
  `id_up_member` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='transaksi klaim komisis member\r\n';

--
-- Dumping data untuk tabel `tr_klaim_komisi_member`
--

INSERT INTO `tr_klaim_komisi_member` (`id_kk_member`, `kd_kk_member`, `nm_properti_member`, `nm_mr_sel`, `nm_mr_pelisting`, `nm_mr_coselling`, `nm_mr_colisting`, `nm_cust_member`, `harga_jual_member`, `komisi_awal_member`, `komisi_member`, `sisa_komisi_member`, `nama_member`, `no_group_member`, `no_rek_bca`, `nama_rek_bca`, `is_del_member`, `crdate_member`, `id_cr_member`, `update_member`, `id_up_member`) VALUES
(1, '3424', 'sdfa 13', 'asdfa 1231', 'sdfa 1231', 'sdfa 131', 'sdfa 131', 'sdfa 3131', '342342', '2342', '2342', '2342', 'afsdf', 'sfa', '23424', 'sdfdas', 0, '2019-08-25 23:54:09', 1, '2019-08-25 23:54:09', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ms_bank`
--
ALTER TABLE `ms_bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indeks untuk tabel `ms_cabang`
--
ALTER TABLE `ms_cabang`
  ADD PRIMARY KEY (`id_cb`);

--
-- Indeks untuk tabel `ms_cara_pembayaran`
--
ALTER TABLE `ms_cara_pembayaran`
  ADD PRIMARY KEY (`id_cp`);

--
-- Indeks untuk tabel `ms_jenis_properti`
--
ALTER TABLE `ms_jenis_properti`
  ADD PRIMARY KEY (`id_jp`);

--
-- Indeks untuk tabel `ms_jenis_transaksi`
--
ALTER TABLE `ms_jenis_transaksi`
  ADD PRIMARY KEY (`id_jt`);

--
-- Indeks untuk tabel `ms_marketing`
--
ALTER TABLE `ms_marketing`
  ADD PRIMARY KEY (`id_mr`);

--
-- Indeks untuk tabel `ms_tim`
--
ALTER TABLE `ms_tim`
  ADD PRIMARY KEY (`id_tim`);

--
-- Indeks untuk tabel `ms_type_marketing`
--
ALTER TABLE `ms_type_marketing`
  ADD PRIMARY KEY (`id_type_mr`);

--
-- Indeks untuk tabel `tbl_hak_akses`
--
ALTER TABLE `tbl_hak_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `tbl_setting`
--
ALTER TABLE `tbl_setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_users`);

--
-- Indeks untuk tabel `tbl_user_level`
--
ALTER TABLE `tbl_user_level`
  ADD PRIMARY KEY (`id_user_level`);

--
-- Indeks untuk tabel `tr_detail_cp_kkm`
--
ALTER TABLE `tr_detail_cp_kkm`
  ADD PRIMARY KEY (`id_detail_kkm`);

--
-- Indeks untuk tabel `tr_detail_jt_kkm`
--
ALTER TABLE `tr_detail_jt_kkm`
  ADD PRIMARY KEY (`id_detail_kkm`);

--
-- Indeks untuk tabel `tr_detail_kkm`
--
ALTER TABLE `tr_detail_kkm`
  ADD PRIMARY KEY (`id_detail_kkm`);

--
-- Indeks untuk tabel `tr_klaim_komisi_marketing`
--
ALTER TABLE `tr_klaim_komisi_marketing`
  ADD PRIMARY KEY (`id_kkm`);

--
-- Indeks untuk tabel `tr_klaim_komisi_member`
--
ALTER TABLE `tr_klaim_komisi_member`
  ADD PRIMARY KEY (`id_kk_member`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ms_bank`
--
ALTER TABLE `ms_bank`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `ms_cabang`
--
ALTER TABLE `ms_cabang`
  MODIFY `id_cb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `ms_cara_pembayaran`
--
ALTER TABLE `ms_cara_pembayaran`
  MODIFY `id_cp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `ms_jenis_properti`
--
ALTER TABLE `ms_jenis_properti`
  MODIFY `id_jp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `ms_jenis_transaksi`
--
ALTER TABLE `ms_jenis_transaksi`
  MODIFY `id_jt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `ms_marketing`
--
ALTER TABLE `ms_marketing`
  MODIFY `id_mr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `ms_tim`
--
ALTER TABLE `ms_tim`
  MODIFY `id_tim` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `ms_type_marketing`
--
ALTER TABLE `ms_type_marketing`
  MODIFY `id_type_mr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tbl_hak_akses`
--
ALTER TABLE `tbl_hak_akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT untuk tabel `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `tbl_setting`
--
ALTER TABLE `tbl_setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tbl_user_level`
--
ALTER TABLE `tbl_user_level`
  MODIFY `id_user_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tr_detail_cp_kkm`
--
ALTER TABLE `tr_detail_cp_kkm`
  MODIFY `id_detail_kkm` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tr_detail_jt_kkm`
--
ALTER TABLE `tr_detail_jt_kkm`
  MODIFY `id_detail_kkm` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tr_detail_kkm`
--
ALTER TABLE `tr_detail_kkm`
  MODIFY `id_detail_kkm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `tr_klaim_komisi_marketing`
--
ALTER TABLE `tr_klaim_komisi_marketing`
  MODIFY `id_kkm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `tr_klaim_komisi_member`
--
ALTER TABLE `tr_klaim_komisi_member`
  MODIFY `id_kk_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

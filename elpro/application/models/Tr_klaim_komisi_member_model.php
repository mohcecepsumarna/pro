<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tr_klaim_komisi_member_model extends CI_Model
{

    public $table = 'tr_klaim_komisi_member';
    public $id = 'id_kk_member';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id_kk_member,kd_kk_member,nm_properti_member,nm_mr_sel,nm_mr_pelisting,nm_mr_coselling,nm_mr_colisting,nm_cust_member,harga_jual_member,komisi_awal_member,komisi_member,sisa_komisi_member,nama_member,no_group_member,no_rek_bca,nama_rek_bca,is_del_member,crdate_member,id_cr_member,update_member,id_up_member');
        $this->datatables->from('tr_klaim_komisi_member');
        //add this line for join
        //$this->datatables->join('table2', 'tr_klaim_komisi_member.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('tr_klaim_komisi_member/read/$1'),'Read')." | ".anchor(site_url('tr_klaim_komisi_member/update/$1'),'Update')." | ".anchor(site_url('tr_klaim_komisi_member/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_kk_member');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_kk_member', $q);
	$this->db->or_like('kd_kk_member', $q);
	$this->db->or_like('nm_properti_member', $q);
	$this->db->or_like('nm_mr_sel', $q);
	$this->db->or_like('nm_mr_pelisting', $q);
	$this->db->or_like('nm_mr_coselling', $q);
	$this->db->or_like('nm_mr_colisting', $q);
	$this->db->or_like('nm_cust_member', $q);
	$this->db->or_like('harga_jual_member', $q);
	$this->db->or_like('komisi_awal_member', $q);
	$this->db->or_like('komisi_member', $q);
	$this->db->or_like('sisa_komisi_member', $q);
	$this->db->or_like('nama_member', $q);
	$this->db->or_like('no_group_member', $q);
	$this->db->or_like('no_rek_bca', $q);
	$this->db->or_like('nama_rek_bca', $q);
	$this->db->or_like('is_del_member', $q);
	$this->db->or_like('crdate_member', $q);
	$this->db->or_like('id_cr_member', $q);
	$this->db->or_like('update_member', $q);
	$this->db->or_like('id_up_member', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_kk_member', $q);
	$this->db->or_like('kd_kk_member', $q);
	$this->db->or_like('nm_properti_member', $q);
	$this->db->or_like('nm_mr_sel', $q);
	$this->db->or_like('nm_mr_pelisting', $q);
	$this->db->or_like('nm_mr_coselling', $q);
	$this->db->or_like('nm_mr_colisting', $q);
	$this->db->or_like('nm_cust_member', $q);
	$this->db->or_like('harga_jual_member', $q);
	$this->db->or_like('komisi_awal_member', $q);
	$this->db->or_like('komisi_member', $q);
	$this->db->or_like('sisa_komisi_member', $q);
	$this->db->or_like('nama_member', $q);
	$this->db->or_like('no_group_member', $q);
	$this->db->or_like('no_rek_bca', $q);
	$this->db->or_like('nama_rek_bca', $q);
	$this->db->or_like('is_del_member', $q);
	$this->db->or_like('crdate_member', $q);
	$this->db->or_like('id_cr_member', $q);
	$this->db->or_like('update_member', $q);
	$this->db->or_like('id_up_member', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ms_cabang_model extends CI_Model
{

    public $table = 'ms_cabang';
    public $id = 'id_cb';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id_cb,kd_cb,nm_cb,ket_cb,is_del_cb,crdate_cb,id_cr_cb,update_cb,id_up_cb');
        $this->datatables->from('ms_cabang');
        //add this line for join
        //$this->datatables->join('table2', 'ms_cabang.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('ms_cabang/update/$1'),'Update')." | ".anchor(site_url('ms_cabang/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_cb');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_cb', $q);
	$this->db->or_like('kd_cb', $q);
	$this->db->or_like('nm_cb', $q);
	$this->db->or_like('ket_cb', $q);
	$this->db->or_like('is_del_cb', $q);
	$this->db->or_like('crdate_cb', $q);
	$this->db->or_like('id_cr_cb', $q);
	$this->db->or_like('update_cb', $q);
	$this->db->or_like('id_up_cb', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_cb', $q);
	$this->db->or_like('kd_cb', $q);
	$this->db->or_like('nm_cb', $q);
	$this->db->or_like('ket_cb', $q);
	$this->db->or_like('is_del_cb', $q);
	$this->db->or_like('crdate_cb', $q);
	$this->db->or_like('id_cr_cb', $q);
	$this->db->or_like('update_cb', $q);
	$this->db->or_like('id_up_cb', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}


<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ms_bank_model extends CI_Model
{

    public $table = 'ms_bank';
    public $id = 'id_bank';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id_bank,kd_bank,nm_bank,namacabang_bank,norek_bank,atasnama_bank,ket_bank,is_del_bank,isaktif_bank,crdate_bank,id_cr_bank,update_bank,id_up_bank');
        $this->datatables->from('ms_bank');
        //add this line for join
        //$this->datatables->join('table2', 'ms_bank.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('ms_bank/update/$1'),'Update')." | ".anchor(site_url('ms_bank/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_bank');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_bank', $q);
	$this->db->or_like('kd_bank', $q);
	$this->db->or_like('nm_bank', $q);
	$this->db->or_like('namacabang_bank', $q);
	$this->db->or_like('norek_bank', $q);
	$this->db->or_like('atasnama_bank', $q);
	$this->db->or_like('ket_bank', $q);
	$this->db->or_like('is_del_bank', $q);
	$this->db->or_like('isaktif_bank', $q);
	$this->db->or_like('crdate_bank', $q);
	$this->db->or_like('id_cr_bank', $q);
	$this->db->or_like('update_bank', $q);
	$this->db->or_like('id_up_bank', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_bank', $q);
	$this->db->or_like('kd_bank', $q);
	$this->db->or_like('nm_bank', $q);
	$this->db->or_like('namacabang_bank', $q);
	$this->db->or_like('norek_bank', $q);
	$this->db->or_like('atasnama_bank', $q);
	$this->db->or_like('ket_bank', $q);
	$this->db->or_like('is_del_bank', $q);
	$this->db->or_like('isaktif_bank', $q);
	$this->db->or_like('crdate_bank', $q);
	$this->db->or_like('id_cr_bank', $q);
	$this->db->or_like('update_bank', $q);
	$this->db->or_like('id_up_bank', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ms_type_marketing_model extends CI_Model
{

    public $table = 'ms_type_marketing';
    public $id = 'id_type_mr';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id_type_mr,kd_type_mr,nm_type_mr,ket_type_mr,is_del_type_mr,crdate_type_mr,id_cr_type_mr,update_type_mr,id_up_type_mr');
        $this->datatables->from('ms_type_marketing');
        //add this line for join
        //$this->datatables->join('table2', 'ms_type_marketing.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('ms_type_marketing/update/$1'),'Update')." | ".anchor(site_url('ms_type_marketing/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_type_mr');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_type_mr', $q);
	$this->db->or_like('kd_type_mr', $q);
	$this->db->or_like('nm_type_mr', $q);
	$this->db->or_like('ket_type_mr', $q);
	$this->db->or_like('is_del_type_mr', $q);
	$this->db->or_like('crdate_type_mr', $q);
	$this->db->or_like('id_cr_type_mr', $q);
	$this->db->or_like('update_type_mr', $q);
	$this->db->or_like('id_up_type_mr', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_type_mr', $q);
	$this->db->or_like('kd_type_mr', $q);
	$this->db->or_like('nm_type_mr', $q);
	$this->db->or_like('ket_type_mr', $q);
	$this->db->or_like('is_del_type_mr', $q);
	$this->db->or_like('crdate_type_mr', $q);
	$this->db->or_like('id_cr_type_mr', $q);
	$this->db->or_like('update_type_mr', $q);
	$this->db->or_like('id_up_type_mr', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

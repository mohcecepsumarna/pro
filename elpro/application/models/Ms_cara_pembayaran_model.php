<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ms_cara_pembayaran_model extends CI_Model
{

    public $table = 'ms_cara_pembayaran';
    public $id = 'id_cp';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id_cp,kd_cp,nm_cp,ket_cp,is_del_cp,crdate_cp,id_cr_cp,update_cp,id_up_cp');
        $this->datatables->from('ms_cara_pembayaran');
        //add this line for join
        //$this->datatables->join('table2', 'ms_cara_pembayaran.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('ms_cara_pembayaran/update/$1'),'Update')." | ".anchor(site_url('ms_cara_pembayaran/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_cp');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_cp', $q);
	$this->db->or_like('kd_cp', $q);
	$this->db->or_like('nm_cp', $q);
	$this->db->or_like('ket_cp', $q);
	$this->db->or_like('is_del_cp', $q);
	$this->db->or_like('crdate_cp', $q);
	$this->db->or_like('id_cr_cp', $q);
	$this->db->or_like('update_cp', $q);
	$this->db->or_like('id_up_cp', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_cp', $q);
	$this->db->or_like('kd_cp', $q);
	$this->db->or_like('nm_cp', $q);
	$this->db->or_like('ket_cp', $q);
	$this->db->or_like('is_del_cp', $q);
	$this->db->or_like('crdate_cp', $q);
	$this->db->or_like('id_cr_cp', $q);
	$this->db->or_like('update_cp', $q);
	$this->db->or_like('id_up_cp', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

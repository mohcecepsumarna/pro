<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ms_marketing_model extends CI_Model
{

    public $table = 'ms_marketing';
    public $table2 = 'ms_type_marketing';
    public $table3 = 'ms_cabang';
    public $table4 = 'ms_tim';
    public $id = 'id_mr';
    public $id2 = 'id_type_mr';
    public $id3 = 'id_cb';
    public $id4 = 'id_tim';
    public $order = 'DESC';
    public $order2 = 'DESC';
    public $order3 = 'DESC';
    public $order4 = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id_mr,kd_mr,nm_mr,alamat_mr,ket_mr,id_type_mrk,id_cb_mrk,is_del_mr,jk_mr,is_active_mr,crdate_mr,id_cr_mr,update_mr,id_up_mr,nm_type_mr,id_type_mr,id_cb,nm_cb,id_tim,nm_tim');
        $this->datatables->from('ms_marketing');
        //add this line for join
        $this->datatables->join($this->table2, 'ms_marketing.id_type_mrk = ms_type_marketing.id_type_mr');
        $this->datatables->join($this->table3, 'ms_marketing.id_cb_mrk = ms_cabang.id_cb');
        $this->datatables->join($this->table4, 'ms_marketing.id_tim_mr = ms_tim.id_tim');
        $this->datatables->add_column('action', anchor(site_url('ms_marketing/update/$1'),'Update')." | ".anchor(site_url('ms_marketing/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_mr');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_mr', $q);
	$this->db->or_like('kd_mr', $q);
	$this->db->or_like('nm_mr', $q);
	$this->db->or_like('alamat_mr', $q);
	$this->db->or_like('ket_mr', $q);
	$this->db->or_like('is_del_mr', $q);
	$this->db->or_like('jk_mr', $q);
	$this->db->or_like('is_active_mr', $q);
	$this->db->or_like('crdate_mr', $q);
	$this->db->or_like('id_cr_mr', $q);
	$this->db->or_like('update_mr', $q);
	$this->db->or_like('id_up_mr', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_mr', $q);
	$this->db->or_like('kd_mr', $q);
	$this->db->or_like('nm_mr', $q);
	$this->db->or_like('alamat_mr', $q);
	$this->db->or_like('ket_mr', $q);
	$this->db->or_like('is_del_mr', $q);
	$this->db->or_like('jk_mr', $q);
	$this->db->or_like('is_active_mr', $q);
	$this->db->or_like('crdate_mr', $q);
	$this->db->or_like('id_cr_mr', $q);
	$this->db->or_like('update_mr', $q);
	$this->db->or_like('id_up_mr', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

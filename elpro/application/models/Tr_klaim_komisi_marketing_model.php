<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tr_klaim_komisi_marketing_model extends CI_Model
{

    public $table = 'tr_klaim_komisi_marketing';
    public $table1 = 'tr_detail_kkm';
    public $table2 = 'tr_detail_jt_kkm';
    public $table3 = 'tr_detail_cp_kkm';
    public $id = 'id_kkm';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id_kkm,kd_kkm,id_jp_kkm,id_jt_kkm,id_cp_kkm,nm_kkm,nm_dev,nm_properti,nm_cust,tgl_terjual,harga_jual,komisi,ket_kkm,is_del_kkm,crdate_kkm,id_cr_kkm,update_kkm,id_up_kkm');
        $this->datatables->from('tr_klaim_komisi_marketing');
        //add this line for join
        //$this->datatables->join('table2', 'tr_klaim_komisi_marketing.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('tr_klaim_komisi_marketing/read/$1'),'Read')." | ".anchor(site_url('tr_klaim_komisi_marketing/delete/$1'),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'), 'id_kkm');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
         // $this->datatables->join('table2', 'tr_klaim_komisi_marketing.field = table2.field');
        // $this->db->join('tr_detail_kkm', 'tr_klaim_komisi_marketing.id_kkm = tr_detail_kkm.id_kkm_detail', 'left');
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_kkm', $q);
        $this->db->or_like('kd_kkm', $q);
        $this->db->or_like('id_jp_kkm', $q);
        $this->db->or_like('id_jt_kkm', $q);
        $this->db->or_like('id_cp_kkm', $q);
        $this->db->or_like('id_mr_kkm', $q);
        $this->db->or_like('nm_kkm', $q);
        $this->db->or_like('nm_dev', $q);
        $this->db->or_like('nm_properti', $q);
        $this->db->or_like('nm_cust', $q);
        $this->db->or_like('tgl_terjual', $q);
        $this->db->or_like('harga_jual', $q);
        $this->db->or_like('komisi', $q);
        $this->db->or_like('ket_kkm', $q);
        $this->db->or_like('is_del_kkm', $q);
        $this->db->or_like('crdate_kkm', $q);
        $this->db->or_like('id_cr_kkm', $q);
        $this->db->or_like('update_kkm', $q);
        $this->db->or_like('id_up_kkm', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_kkm', $q);
        $this->db->or_like('kd_kkm', $q);
        $this->db->or_like('id_jp_kkm', $q);
        $this->db->or_like('id_jt_kkm', $q);
        $this->db->or_like('id_cp_kkm', $q);
        $this->db->or_like('id_mr_kkm', $q);
        $this->db->or_like('nm_kkm', $q);
        $this->db->or_like('nm_dev', $q);
        $this->db->or_like('nm_properti', $q);
        $this->db->or_like('nm_cust', $q);
        $this->db->or_like('tgl_terjual', $q);
        $this->db->or_like('harga_jual', $q);
        $this->db->or_like('komisi', $q);
        $this->db->or_like('ket_kkm', $q);
        $this->db->or_like('is_del_kkm', $q);
        $this->db->or_like('crdate_kkm', $q);
        $this->db->or_like('id_cr_kkm', $q);
        $this->db->or_like('update_kkm', $q);
        $this->db->or_like('id_up_kkm', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    } 

    // insert data
    function insert1($data)
    {
        $this->db->insert($this->table1, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

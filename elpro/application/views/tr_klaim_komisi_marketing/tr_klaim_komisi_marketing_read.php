<!-- 
        <h2 style="margin-top:0px">Tr_klaim_komisi_marketing Read</h2>
        <table class="table">
	    <tr><td>Kd Kkm</td><td><?php echo $kd_kkm; ?></td></tr>
	    <tr><td>Id Jp Kkm</td><td><?php echo $id_jp_kkm; ?></td></tr>
	    <tr><td>Id Jt Kkm</td><td><?php echo $id_jt_kkm; ?></td></tr>
	    <tr><td>Id Cp Kkm</td><td><?php echo $id_cp_kkm; ?></td></tr>
	    <tr><td>Id Mr Kkm</td><td><?php echo $id_mr_kkm; ?></td></tr>
	    <tr><td>Nm Kkm</td><td><?php echo $nm_kkm; ?></td></tr>
	    <tr><td>Nm Dev</td><td><?php echo $nm_dev; ?></td></tr>
	    <tr><td>Nm Properti</td><td><?php echo $nm_properti; ?></td></tr>
	    <tr><td>Nm Cust</td><td><?php echo $nm_cust; ?></td></tr>
	    <tr><td>Tgl Terjual</td><td><?php echo $tgl_terjual; ?></td></tr>
	    <tr><td>Harga Jual</td><td><?php echo $harga_jual; ?></td></tr>
	    <tr><td>Komisi</td><td><?php echo $komisi; ?></td></tr>
	    <tr><td>Ket Kkm</td><td><?php echo $ket_kkm; ?></td></tr>
	    <tr><td>Is Del Kkm</td><td><?php echo $is_del_kkm; ?></td></tr>
	    <tr><td>Crdate Kkm</td><td><?php echo $crdate_kkm; ?></td></tr>
	    <tr><td>Id Cr Kkm</td><td><?php echo $id_cr_kkm; ?></td></tr>
	    <tr><td>Update Kkm</td><td><?php echo $update_kkm; ?></td></tr>
	    <tr><td>Id Up Kkm</td><td><?php echo $id_up_kkm; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('tr_klaim_komisi_marketing') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
-->

<div class="content-wrapper">

	<section class="content">


		<div class="box box-warning box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">FORM KLAIM KOMISI MARKETING</h3>
			</div>
			
			<table class="table table-condensed" border="1">
				<tr>
					<td colspan="5" class="text-center"><h4>FORM KLAIM KOMISI MARKETING</h4></td>
				</tr>
			</table>

			<table class="table">
				<thead>
					<tr>
						<th></th>
						<th>Jenis Properti</th>
						<th>Jenis Transaksi</th>
						<th>Cara Pembayaran</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($details as $key => $value): ?>

						<tr>
							<td></td>
							<td>
								<?php 
								if ($value->id_jp_dkkm == 1) {
									echo "Rumah";
								}if ($value->id_jp_dkkm == 2) {
									echo "Tanah";
								}if ($value->id_jp_dkkm == 3) {
									echo "Ruko";
								}if ($value->id_jp_dkkm == 4) {
									echo "Apartemen";
								}if ($value->id_jp_dkkm == 5) {
									echo $value->dll_kkm;
	        				// echo "&nbsp;,Lainnya";
								} else {
									echo "";
								}

								?>
							</td>
							<td><?php if ($value->id_jt_dkkm == 1) {
								echo "Jual";
							} if($value->id_jt_dkkm == 2) {
								echo "Primary";
							} if($value->id_jt_dkkm == 3) {
								echo "Sewa";
							} if($value->id_jt_dkkm == 4) {
								echo "Secondary";
							} else {
								echo "";
							}
							?></td>
							<td><?php 
							if ($value->id_cp_dkkm == 1) {
								echo " Cash Keras";
							}if ($value->id_cp_dkkm == 2) {
								echo "KPR Konvensional";
							}if ($value->id_cp_dkkm == 3) {
								echo "Cash Bertahap";
							}if ($value->id_cp_dkkm == 4) {
								echo "KPR Syariah";
							}if ($value->id_cp_dkkm == 5) {
								echo "Cicil Developer";
							} else {
								echo "";
							}

							?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div class="container">
				<table class="table">
					<tr>
						<td><h4 class="box-title">DETAIL TRANSAKSI</h4></td>
					</tr>


					<tr>
						<td width="250px;">Nama Pemilik/Developer: </td>
						<td><?php echo $datas->nm_dev; ?> </td>
					</tr>
					<tr>
						<td width="250px;">Nama Properti: </td>
						<td><?php echo $datas->nm_properti; ?> </td>
					</tr>
					<tr>
						<td width="250px;">Alamat Properti: </td>
						<td><?php echo $datas->ket_kkm; ?> </td>
					</tr>
					<tr>
						<td width="250px;">Nama Pembeli: </td>
						<td><?php echo $datas->nm_cust; ?> </td>
					</tr>
					<tr>
						<td width="250px;">Tanggal Terjual: </td>
						<td><?php echo $datas->tgl_terjual; ?> </td>
					</tr>
					<tr>
						<td width="250px;">Harga Terjual: </td>
						<td><?php echo $datas->harga_jual; ?> </td>
					</tr>
					<tr>
						<td width="250px;">Komisi: </td>
						<td><?php echo $datas->komisi; ?> </td>
					</tr>

				</table>
			</div>
			<div class="box box-primary">
				<div class="box-header with-border">
					<h5 class="box-title">Marketing</h5>
				</div>
				<div class="box-body">
					<!-- <div class="form-group"> -->

						<div class="col-sm-6">
							<label>Selling</label>
							<table>
								<tr>
									<td width="200px;">Nama</td>
									<td>: <?php echo $ms_mark->nm_mr; ?></td>
								</tr><tr>
									<td width="200px;">ID</td>
									<td>: </td>
								</tr><tr>
									<td width="200px;">Cabang Kantor</td>
									<td>: <?php echo $ms_mark->nm_cb; ?></td>
								</tr><tr>
									<td width="200px;">No. Di Rek BCA</td>
									<td>: </td>
								</tr><tr>
									<td width="200px;">Nama Di Rek BCA</td>
									<td>: </td>
								</tr>
							</table>

						</div>
						<div class="col-sm-6">
							<label>Listing </label>
							<table>
								<tr>
									<td width="200px;">Nama</td>
									<td>: <?php echo $ms_mark->nm_mr; ?></td>
								</tr><tr>
									<td width="200px;">ID</td>
									<td>: </td>
								</tr><tr>
									<td width="200px;">Cabang Kantor</td>
									<td>: <?php echo $ms_mark->nm_cb; ?></td>
								</tr><tr>
									<td width="200px;">No. Di Rek BCA</td>
									<td>: </td>
								</tr><tr>
									<td width="200px;">Nama Di Rek BCA</td>
									<td>: </td>
								</tr>
							</table>

						</div>
						<div class="col-sm-6" style="padding-top: 15px;">
							<label>Co-Selling <?php echo form_error('id_mr_cosel') ?></label>
							<table>
								<tr>
									<td width="200px;">Nama</td>
									<td>: <?php echo $ms_mark->nm_mr; ?></td>
								</tr><tr>
									<td width="200px;">ID</td>
									<td>: </td>
								</tr><tr>
									<td width="200px;">Cabang Kantor</td>
									<td>: <?php echo $ms_mark->nm_cb; ?></td>
								</tr><tr>
									<td width="200px;">No. Di Rek BCA</td>
									<td>: </td>
								</tr><tr>
									<td width="200px;">Nama Di Rek BCA</td>
									<td>: </td>
								</tr>
							</table>

						</div>
						<div class="col-sm-6" style="padding-top: 15px;">
							<label>Co-Listing <?php echo form_error('id_mr_colis') ?></label>
							<table>
								<tr>
									<td width="200px;">Nama</td>
									<td>: <?php echo $ms_mark->nm_mr; ?></td>
								</tr><tr>
									<td width="200px;">ID</td>
									<td>: </td>
								</tr><tr>
									<td width="200px;">Cabang Kantor</td>
									<td>: <?php echo $ms_mark->nm_cb; ?></td>
								</tr><tr>
									<td width="200px;">No. Di Rek BCA</td>
									<td>: </td>
								</tr><tr>
									<td width="200px;">Nama Di Rek BCA</td>
									<td>: </td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				
				<div class="container-fluid">
				<table class="table" width="100%" border="1">
					<thead>
						<tr>
							<th class="text-center" width="250px;">Selling /  Co-Selling</th>
							<th class="text-center" width="250px;">Listing / Co-Listing</th>
							<th class="text-center" width="250px;">Admin</th>
							<th class="text-center" width="250px;">Kepala Cabang</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><br><br><br></td>
							<td><br><br><br></td>
							<td><br><br><br></td>
							<td><br><br><br></td>
						</tr>
					</tbody>
				</table>
				<table>
					<a href="<?php echo site_url('tr_klaim_komisi_marketing') ?>" class="btn btn-default">Cancel</a>
				</table>
				</div>

			</div>
		</section>
	</div>
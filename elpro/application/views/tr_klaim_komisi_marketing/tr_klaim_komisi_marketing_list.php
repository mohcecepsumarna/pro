<div class="content-wrapper">
   <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning box-solid">
        
                <div class="box-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h3 class="box-title">Data Klaim Komisi Marketing</h3>
                        </div>
                        <div class="col-md-4 text-right">
                            <?php echo anchor(site_url('tr_klaim_komisi_marketing/create'),'<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-danger btn-sm"'); ?>
                        </div>
                    </div>
                </div>

        <div class="box-body">
            <div style="margin-top: 4px"  id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
            <table class="table table-bordered table-striped" id="mytable">
                <thead>
                    <tr>
                        <th width="80px">No</th>
                        <th>Kode</th>
                        <!-- <th>Jenis Properti</th> -->
                        <!-- <th>Jenis Transaksi</th> -->
                        <!-- <th>Cara Pembayaran</th> -->
                        <!-- <th>Marketing</th> -->
                        <!-- <th>Nm Kkm</th> -->
                        <th>Nama Developer</th>
                        <th>Nama Properti</th>
                        <th>Nama Cust</th>
                        <th>Tgl Terjual</th>
                        <th>Harga Jual</th>
                        <th>Komisi</th>
                        <!-- <th>Ket Kkm</th> -->
                        <th width="200px">Action</th>
                    </tr>
                </thead>
            </table>
        </div>

        </div>
    </div>
</div>
</section>
</div>

        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#mytable").dataTable({
                    // initComplete: function() {
                    //     var api = this.api();
                    //     $('#mytable_filter input')
                    //             .off('.DT')
                    //             .on('keyup.DT', function(e) {
                    //                 if (e.keyCode == 13) {
                    //                     api.search(this.value).draw();
                    //         }
                    //     });
                    // },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "tr_klaim_komisi_marketing/json", "type": "POST"},
                    columns: [
                        {
                            "data": "id_kkm",
                            "orderable": false
                        },
                        {"data": "kd_kkm"},
                        // {"data": "id_jp_kkm"},
                        // {"data": "id_jt_kkm"},
                        // {"data": "id_cp_kkm"},
                        // {"data": "id_mr_kkm"},
                        // {"data": "nm_kkm"},
                        {"data": "nm_dev"},
                        {"data": "nm_properti"},
                        {"data": "nm_cust"},
                        {"data": "tgl_terjual"},
                        {"data": "harga_jual"},
                        {"data": "komisi"},
                        // {"data": "ket_kkm"},
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center"
                        }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>

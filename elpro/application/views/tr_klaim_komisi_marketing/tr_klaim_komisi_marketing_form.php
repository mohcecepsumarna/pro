<div class="content-wrapper">

  <section class="content">
    <div class="box box-warning box-solid">
      <div class="box-header with-border">
        <h3 class="box-title"><a><?php echo $title ?></a> Klaim Komisi Marketing</h3>
      </div>

      <div class="box-body box-info">

        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label">Kode <?php echo form_error('kd_kkm') ?></label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="kd_kkm" id="kd_kkm" placeholder="Kode" value="<?php echo $kd_kkm; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Jenis Properti <?php echo form_error('id_jp_kkm') ?></label>
            <div class="col-sm-10">
              <div class="row">
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jp_kkm[]" value="1">
                      Rumah
                    </label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jp_kkm[]" value="2">
                      Tanah
                    </label>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jp_kkm[]" value="3">
                      Ruko
                    </label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jp_kkm[]" value="4">
                      Apartemen
                    </label>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jp_kkm[]" value="5">
                      Lainnya
                    </label>
                    <input type="text" class="form-control" name="dll_kkm" id="dll_kkm" placeholder="Lainnya" value="<?php echo $dll_kkm; ?>" />
                  </div>

                </div>
              </div>
            </div>

          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Jenis Transaksi <?php echo form_error('id_jt_kkm') ?></label>
            <div class="col-sm-10">
              <div class="row">
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jt_kkm[]" value="1">
                      Jual
                    </label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jt_kkm[]" value="2">
                      Primary
                    </label>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jt_kkm[]" value="3">
                      Sewa
                    </label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_jt_kkm[]" value="4">
                      Secondary
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Cara Pembayaran <?php echo form_error('id_cp_kkm') ?></label>
            <div class="col-sm-10">
              <div class="row">
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_cp_kkm[]" value="1">
                      Cash Keras
                    </label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_cp_kkm[]" value="2">
                      KPR Konvensional
                    </label>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_cp_kkm[]" value="3">
                      Cash Bertahap
                    </label>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_cp_kkm[]" value="4">
                      KPR Syariah
                    </label>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="id_cp_kkm[]" value="5">
                      Cicil Developer
                    </label>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <!-- START DETAIL TRANSAKSI -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h5 class="box-title">Detail Transaksi</h5>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Nama Developer <?php echo form_error('nm_dev') ?></label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="nm_dev" id="nm_dev" placeholder="Nama Developer" value="<?php echo $nm_dev; ?>" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Nama Properti <?php echo form_error('nm_properti') ?></label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="nm_properti" id="nm_properti" placeholder="Nama Properti" value="<?php echo $nm_properti; ?>" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Alamat Properti <?php echo form_error('ket_kkm') ?></label>
                <div class="col-sm-6">
                  <textarea class="form-control" rows="3" name="ket_kkm" id="ket_kkm" placeholder="ALamat Properti"><?php echo $ket_kkm; ?></textarea>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Nama Pembeli <?php echo form_error('nm_cust') ?></label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="nm_cust" id="nm_cust" placeholder="Nama Pembeli" value="<?php echo $nm_cust; ?>" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal Terjual <?php echo form_error('tgl_terjual') ?></label>
                <div class="col-sm-4">
                  <input type="date" class="form-control" name="tgl_terjual" id="tgl_terjual" placeholder="Tgl Terjual" value="<?php echo $tgl_terjual; ?>" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Harga Jual <?php echo form_error('harga_jual') ?></label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="harga_jual" id="harga_jual" placeholder="Harga Jual" value="<?php echo $harga_jual; ?>" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Komisi <?php echo form_error('komisi') ?></label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="komisi" id="komisi" placeholder="Komisi" value="<?php echo $komisi; ?>" />
                </div>
              </div>
            </div>
          </div>
          <!-- END Transaksi -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h5 class="box-title">Marketing</h5>
            </div>
            <div class="box-body">
              <div class="form-group">

                <div class="col-sm-6">
                  <label>Selling <?php echo form_error('id_mr_sel') ?></label>
                  <select name="id_mr_sel" class="form-control">
                    <option value=""></option>
                    <?php foreach ($mark_select_values as $msvalue): if($msvalue->id_type_mrk == 1){ ?>
                      <option value="<?=$msvalue->id_mr?>" <?php if ($id_mr_sel == $msvalue->id_mr) {
                        echo "selected";
                      }}
                      ?>>
                      <?php echo ucwords($msvalue->nm_mr) ?>
                    </option>
                  <?php endforeach?>
                </select>
                
              </div>
              <div class="col-sm-6">
                <label>Listing <?php echo form_error('id_mr_lis') ?></label>
                <select name="id_mr_lis" class="form-control">
                  <option value=""></option>
                  <?php foreach ($mark_select_values as $msvalue): if($msvalue->id_type_mrk == 2){ ?>
                    <option value="<?=$msvalue->id_mr?>" <?php if ($id_mr_lis == $msvalue->id_mr) {
                      echo "selected";
                    }}
                    ?>>
                    <?php echo ucwords($msvalue->nm_mr) ?>
                  </option>
                <?php endforeach?>
              </select>

            </div>
            <div class="col-sm-6" style="padding-top: 15px;">
              <label>Co-Selling <?php echo form_error('id_mr_cosel') ?></label>
              <select name="id_mr_cosel" class="form-control">
                <option value=""></option>
                <?php foreach ($mark_select_values as $msvalue): if($msvalue->id_type_mrk == 4){ ?>
                  <option value="<?=$msvalue->id_mr?>" <?php if ($id_mr_cosel == $msvalue->id_mr) {
                    echo "selected";
                  }}
                  ?>>
                  <?php echo ucwords($msvalue->nm_mr) ?>
                </option>
              <?php endforeach?>
            </select>

          </div>
          <div class="col-sm-6" style="padding-top: 15px;">
            <label>Co-Listing <?php echo form_error('id_mr_colis') ?></label>
            <select name="id_mr_colis" class="form-control">
              <option value=""></option>
              <?php foreach ($mark_select_values as $msvalue): if($msvalue->id_type_mrk == 3){ ?>
                <option value="<?=$msvalue->id_mr?>" <?php if ($id_mr_colis == $msvalue->id_mr) {
                  echo "selected";
                }}
                ?>>
                <?php echo ucwords($msvalue->nm_mr) ?>
              </option>
            <?php endforeach?>
          </select>

        </div>
      </div>


    </div>
  </div>
  <div class="box-footer text-center">
    <input type="hidden" name="id_kkm" value="<?php echo $id_kkm; ?>" /> 
    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
    <a href="<?php echo site_url('tr_klaim_komisi_marketing') ?>" class="btn btn-default">Cancel</a>
  </div>
</form>

</div>
</div>
</section>
</div>
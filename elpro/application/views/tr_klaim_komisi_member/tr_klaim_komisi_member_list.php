<div class="content-wrapper">
 <section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="box box-warning box-solid">
            
            <div class="box-header">
                <div class="row">
                    <div class="col-md-8">
                      <h3 class="box-title">Data Klaim Komisi Member</h3>
                  </div>
                  
                  <div class="col-md-4 text-right">
                    <?php echo anchor(site_url('tr_klaim_komisi_member/create'),'<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-danger btn-sm"'); ?>
                </div>
            </div>
        </div>

        <div class="box-body">
           <div style="margin-top: 4px"  id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
                    <th>Kode</th>
                    <th>Nama Properti</th>
                  <!--   <th>Selling</th>
                    <th>Pelisting</th>
                    <th>Coselling</th>
                    <th>Colisting</th> -->
                    <th>Nama Pembeli</th>
                    <th>Harga Jual Member</th>
                    <th>Komisi Awal Member</th>
                    <th>Komisi Member</th>
                    <th>Sisa Komisi Member</th>
                    <th>Nama Member</th>
                    <th width="200px">Action</th>
                </tr>
            </thead>

        </table>
    </div>
</div>
</div>
</div>
</section>
</div>

<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
                    // initComplete: function() {
                    //     var api = this.api();
                    //     $('#mytable_filter input')
                    //             .off('.DT')
                    //             .on('keyup.DT', function(e) {
                    //                 if (e.keyCode == 13) {
                    //                     api.search(this.value).draw();
                    //         }
                    //     });
                    // },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "tr_klaim_komisi_member/json", "type": "POST"},
                    columns: [
                    {
                        "data": "id_kk_member",
                        "orderable": false
                    },{"data": "kd_kk_member"},
                    {"data": "nm_properti_member"},
                    // {"data": "nm_mr_sel"},
                    // {"data": "nm_mr_pelisting"},
                    // {"data": "nm_mr_coselling"},
                    // {"data": "nm_mr_colisting"},
                    {"data": "nm_cust_member"},
                    {"data": "harga_jual_member"},
                    {"data": "komisi_awal_member"},
                    {"data": "komisi_member"},
                    {"data": "sisa_komisi_member"},
                    {"data": "nama_member"},
                    {
                        "data" : "action",
                        "orderable": false,
                        "className" : "text-center"
                    }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
    });
</script>

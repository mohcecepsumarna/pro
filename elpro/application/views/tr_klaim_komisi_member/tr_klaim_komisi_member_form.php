<div class="content-wrapper">

  <section class="content">
    <div class="box box-warning box-solid">
      <div class="box-header with-border">
        <h3 class="box-title"><a><?php echo $title ?></a> Klaim Komisi Marketing</h3>
    </div>

    <div class="box-body box-info">
        <form action="<?php echo $action; ?>" method="post">
           <div class="form-group">
            <label for="varchar">Kode Member <?php echo form_error('kd_kk_member') ?></label>
            <input type="text" class="form-control" name="kd_kk_member" id="kd_kk_member" placeholder="Kode" value="<?php echo $kd_kk_member; ?>" />
        </div>

        <div class="box box-danger">
            <div class="box-header with-border">
              <h5 class="box-title">Detail Transaksi</h5>
          </div>
          <div class="box-body">
           <div class="form-group">
            <label for="varchar">Nama Properti Member <?php echo form_error('nm_properti_member') ?></label>
            <input type="text" class="form-control" name="nm_properti_member" id="nm_properti_member" placeholder="Nama Properti Member" value="<?php echo $nm_properti_member; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Nama Marketing Selling <?php echo form_error('nm_mr_sel') ?></label>
            <input type="text" class="form-control" name="nm_mr_sel" id="nm_mr_sel" placeholder="Nama Marketing Selling" value="<?php echo $nm_mr_sel; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Nama Marketing Pelisting <?php echo form_error('nm_mr_pelisting') ?></label>
            <input type="text" class="form-control" name="nm_mr_pelisting" id="nm_mr_pelisting" placeholder="Nama Marketing Pelisting" value="<?php echo $nm_mr_pelisting; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Nama Marketing Coselling <?php echo form_error('nm_mr_coselling') ?></label>
            <input type="text" class="form-control" name="nm_mr_coselling" id="nm_mr_coselling" placeholder="Nama Marketing Coselling" value="<?php echo $nm_mr_coselling; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Nama Marketing Colisting <?php echo form_error('nm_mr_colisting') ?></label>
            <input type="text" class="form-control" name="nm_mr_colisting" id="nm_mr_colisting" placeholder="Nama Marketing Colisting" value="<?php echo $nm_mr_colisting; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Nama Pembeli <?php echo form_error('nm_cust_member') ?></label>
            <input type="text" class="form-control" name="nm_cust_member" id="nm_cust_member" placeholder="Nama Pembeli" value="<?php echo $nm_cust_member; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Harga Jual Member <?php echo form_error('harga_jual_member') ?></label>
            <input type="text" class="form-control" name="harga_jual_member" id="harga_jual_member" placeholder="Harga Jual Member" value="<?php echo $harga_jual_member; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Komisi Awal Member <?php echo form_error('komisi_awal_member') ?></label>
            <input type="text" class="form-control" name="komisi_awal_member" id="komisi_awal_member" placeholder="Komisi Awal Member" value="<?php echo $komisi_awal_member; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Komisi Member <?php echo form_error('komisi_member') ?></label>
            <input type="text" class="form-control" name="komisi_member" id="komisi_member" placeholder="Komisi Member" value="<?php echo $komisi_member; ?>" />
        </div>
        <div class="form-group">
            <label for="varchar">Sisa Komisi Member <?php echo form_error('sisa_komisi_member') ?></label>
            <input type="text" class="form-control" name="sisa_komisi_member" id="sisa_komisi_member" placeholder="Sisa Komisi Member" value="<?php echo $sisa_komisi_member; ?>" />
        </div>
    </div>
</div>

<div class="box box-danger">
    <div class="box-header with-border">
      <h5 class="box-title">Data Member</h5>
  </div>
  <div class="box-body">
   <div class="form-group">
    <label for="varchar">Nama Member <?php echo form_error('nama_member') ?></label>
    <input type="text" class="form-control" name="nama_member" id="nama_member" placeholder="Nama Member" value="<?php echo $nama_member; ?>" />
</div>
<div class="form-group">
    <label for="varchar">No Group Member <?php echo form_error('no_group_member') ?></label>
    <input type="text" class="form-control" name="no_group_member" id="no_group_member" placeholder="No Group Member" value="<?php echo $no_group_member; ?>" />
</div>
<div class="form-group">
    <label for="varchar">No Rek Bca <?php echo form_error('no_rek_bca') ?></label>
    <input type="text" class="form-control" name="no_rek_bca" id="no_rek_bca" placeholder="No Rek Bca" value="<?php echo $no_rek_bca; ?>" />
</div>
<div class="form-group">
    <label for="varchar">Nama Rek Bca <?php echo form_error('nama_rek_bca') ?></label>
    <input type="text" class="form-control" name="nama_rek_bca" id="nama_rek_bca" placeholder="Nama Rek Bca" value="<?php echo $nama_rek_bca; ?>" />
</div>
</div>
</div>

<div class="box-footer text-center">
   <input type="hidden" name="id_kk_member" value="<?php echo $id_kk_member; ?>" /> 
   <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
   <a href="<?php echo site_url('tr_klaim_komisi_member') ?>" class="btn btn-default">Cancel</a>
</div>
</form>
</div>
</div>
</section>
</div>
<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><a><?php echo $title ?></a> Cabang</h3>
            </div>

            <!-- <h2 style="margin-top:0px">Ms_cabang</h2> -->
            <div class="box-body box-info">
              <form action="<?php echo $action; ?>" method="post" class="form-horizontal">        
                <div class="form-group">
                    <label class="col-sm-2 control-label">Kode <?php echo form_error('kd_cb') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kd_cb" id="kd_cb" placeholder="Kode" value="<?php echo $kd_cb; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama <?php echo form_error('nm_cb') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nm_cb" id="nm_cb" placeholder="Nama" value="<?php echo $nm_cb; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan <?php echo form_error('ket_cb') ?></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="3" name="ket_cb" id="ket_cb" placeholder="Keterangan"><?php echo $ket_cb; ?></textarea>
                    </div>
                </div>

                <input type="hidden" name="id_cb" value="<?php echo $id_cb; ?>" /> 
                
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('ms_cabang') ?>" class="btn btn-default">Cancel</a>
                </div>
            </form>
        </div>

    </div>
</section>
</div>

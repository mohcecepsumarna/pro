<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><a><?php echo $title ?></a> Marketing</h3>
            </div>
            
            <div class="box-body box-info">

                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                   <div class="form-group">
                    <label class="col-sm-2 control-label">kode Marketing <?php echo form_error('kd_mr') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kd_mr" id="kd_mr" placeholder="Kode Marketing" value="<?php echo $kd_mr; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Marketing <?php echo form_error('nm_mr') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nm_mr" id="nm_mr" placeholder="Nama Marketing" value="<?php echo $nm_mr; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Type Marketing <?php echo form_error('id_type_mrk') ?></label>
                    <div class="col-sm-10">
                        <select name="id_type_mrk" class="form-control">
                                <?php foreach ($mark_select_values as $msvalue): ?>
                                  <option value="<?=$msvalue->id_type_mr?>" <?php if ($id_type_mrk == $msvalue->id_type_mr) {
                                    echo "selected";
                                }
                                ?>>
                                <?php echo ucwords($msvalue->nm_type_mr) ?>
                            </option>
                        <?php endforeach?>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Mitra <?php echo form_error('id_cb_mrk') ?></label>
                    <div class="col-sm-10">
                        <select name="id_cb_mrk" class="form-control">
                                <?php foreach ($mtr_select_values as $msvalue): ?>
                                  <option value="<?=$msvalue->id_cb?>" <?php if ($id_cb_mrk == $msvalue->id_cb) {
                                    echo "selected";
                                }
                                ?>>
                                <?php echo ucwords($msvalue->nm_cb) ?>
                            </option>
                        <?php endforeach?>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tim <?php echo form_error('id_tim_mr') ?></label>
                    <div class="col-sm-10">
                        <select name="id_tim_mr" class="form-control">
                                <?php foreach ($tim_select_values as $msvalue): ?>
                                  <option value="<?=$msvalue->id_tim?>" <?php if ($id_tim_mr == $msvalue->id_tim) {
                                    echo "selected";
                                }
                                ?>>
                                <?php echo ucwords($msvalue->nm_tim) ?>
                            </option>
                        <?php endforeach?>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Alamat Marketing <?php echo form_error('alamat_mr') ?></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="3" name="alamat_mr" id="alamat_mr" placeholder="Alamat Marketing"><?php echo $alamat_mr; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan <?php echo form_error('ket_mr') ?></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="3" name="ket_mr" id="ket_mr" placeholder="Keterangan"><?php echo $ket_mr; ?></textarea>
                    </div>
                </div>

                <div class="box-footer text-center">
                    <input type="hidden" name="id_mr" value="<?php echo $id_mr; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('ms_marketing') ?>" class="btn btn-default">Cancel</a>
                </div>
            </form>

        </div>
    </div>
</section>
</div>
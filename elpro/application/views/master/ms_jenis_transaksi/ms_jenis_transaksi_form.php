<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><a><?php echo $title ?></a> Jenis Transaksi</h3>
            </div>
            
            <div class="box-body box-info">

                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                   <div class="form-group">
                    <label class="col-sm-2 control-label">Kode <?php echo form_error('kd_jt') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="kd_jt" id="kd_jt" placeholder="Kode" value="<?php echo $kd_jt; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama <?php echo form_error('nm_jt') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="nm_jt" id="nm_jt" placeholder="Nama" value="<?php echo $nm_jt; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan <?php echo form_error('ket_jt') ?></label>
                    <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="ket_jt" id="ket_jt" placeholder="Keterangan"><?php echo $ket_jt; ?></textarea>
                </div>
                </div>
                <div class="box-footer text-center">
                   <input type="hidden" name="id_jt" value="<?php echo $id_jt; ?>" /> 
                   <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                   <a href="<?php echo site_url('ms_jenis_transaksi') ?>" class="btn btn-default">Cancel</a>
               </div>
           </form>
       </div>
   </div>
</section>
</div>

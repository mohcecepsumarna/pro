<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><a><?php echo $title ?></a> Tim</h3>
            </div>
            
            <div class="box-body box-info">

                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                   <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Tim <?php echo form_error('kd_tim') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kd_tim" id="kd_tim" placeholder="Kode Tim" value="<?php echo $kd_tim; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Tim <?php echo form_error('nm_tim') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nm_tim" id="nm_tim" placeholder="Nama Tim" value="<?php echo $nm_tim; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keteranga <?php echo form_error('ket_tim') ?></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="3" name="ket_tim" id="ket_tim" placeholder="Keterangan Tim"><?php echo $ket_tim; ?></textarea>
                    </div>
                </div>
                
                <div class="box-footer text-center">
                    <input type="hidden" name="id_tim" value="<?php echo $id_tim; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('ms_tim') ?>" class="btn btn-default">Cancel</a>
                </div>
            </form>


        </div>
    </div>
</section>
</div>
<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><a><?php echo $title ?></a> Bank</h3>
            </div>
            
            <div class="box-body box-info">

                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                   <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Bank <?php echo form_error('kd_bank') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="kd_bank" id="kd_bank" placeholder="Kode Bank" value="<?php echo $kd_bank; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Bank <?php echo form_error('nm_bank') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="nm_bank" id="nm_bank" placeholder="Nama Bank" value="<?php echo $nm_bank; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Cabang Bank <?php echo form_error('namacabang_bank') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="namacabang_bank" id="namacabang_bank" placeholder="Nama Cabang Bank" value="<?php echo $namacabang_bank; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Norek Bank <?php echo form_error('norek_bank') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="norek_bank" id="norek_bank" placeholder="Norek Bank" value="<?php echo $norek_bank; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Atas Nama Bank <?php echo form_error('atasnama_bank') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="atasnama_bank" id="atasnama_bank" placeholder="Atas Nama Bank" value="<?php echo $atasnama_bank; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Ket Bank <?php echo form_error('ket_bank') ?></label>
                    <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="ket_bank" id="ket_bank" placeholder="Ket Bank"><?php echo $ket_bank; ?></textarea>
                </div>
                </div>
                <div class="box-footer text-center">
                <input type="hidden" name="id_bank" value="<?php echo $id_bank; ?>" /> 
                <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                <a href="<?php echo site_url('ms_bank') ?>" class="btn btn-default">Cancel</a>
                 </div>
            </form>

        </div>
    </div>
</section>
</div>


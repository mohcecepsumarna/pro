<div class="content-wrapper">
   <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-warning box-solid">

            <div class="box-header">
                <div class="col-md-8">
                    <h3 class="box-title">Data Bank</h3>
                </div>
                <div class="col-md-4 text-right">
                    <?php echo anchor(site_url('ms_bank/create'),'<i class="fa fa-wpforms" aria-hidden="true"></i> Tambah Data', 'class="btn btn-danger btn-sm"'); ?>
                </div>
            </div>

            <div class="box-body">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
                <table class="table table-bordered table-striped" id="mytable">
                    <thead>
                        <tr>
                            <th width="80px">No</th>
                            <th>Kode Bank</th>
                            <th>Nama Bank</th>
                            <th>Nama Cabang Bank</th>
                            <th>No Rekening Bank</th>
                            <th>Atas Nama Bank</th>
                            <th>Keterangan Bank</th>
                            <th width="200px">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
</section>
</div>

<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "ms_bank/json", "type": "POST"},
            columns: [
            {
                "data": "id_bank",
                "orderable": false
            },
            {"data": "kd_bank"},
            {"data": "nm_bank"},
            {"data": "namacabang_bank"},
            {"data": "norek_bank"},
            {"data": "atasnama_bank"},
            {"data": "ket_bank"},
            {
                "data" : "action",
                "orderable": false,
                "className" : "text-center"
            }
            ],
            order: [[0, 'desc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
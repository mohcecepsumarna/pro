<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><a><?php echo $title ?></a> Cara Pembayaran</h3>
            </div>
            <!-- <h2 style="margin-top:0px">Ms_jenis_properti <?php echo $button ?></h2> -->
            <div class="box-body box-info">
                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                   <div class="form-group">
                    <label class="col-sm-2 control-label">Kode <?php echo form_error('kd_jp') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kd_jp" id="kd_jp" placeholder="Kode" value="<?php echo $kd_jp; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama <?php echo form_error('nm_jp') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="nm_jp" id="nm_jp" placeholder="Nama" value="<?php echo $nm_jp; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan <?php echo form_error('ket_jp') ?></label>
                    <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="ket_jp" id="ket_jp" placeholder="Keterangan"><?php echo $ket_jp; ?></textarea>
                </div>
                </div>
                <div class="box-footer text-center">
                   <input type="hidden" name="id_jp" value="<?php echo $id_jp; ?>" /> 
                   <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                   <a href="<?php echo site_url('ms_jenis_properti') ?>" class="btn btn-default">Cancel</a>
               </div>
           </form>
       </div>
   </div>
</section>
</div>


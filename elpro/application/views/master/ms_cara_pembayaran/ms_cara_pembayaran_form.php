<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><a><?php echo $title ?></a> Cara Pembayaran</h3>
            </div>

            <!-- <h2 style="margin-top:0px">Ms_cabang</h2> -->
            <div class="box-body box-info">
                <!-- <h2 style="margin-top:0px">Ms_cara_pembayaran <a><?php echo $button ?></a></h2> -->
                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                 <div class="form-group">
                    <label class="col-sm-2 control-label">Kode <?php echo form_error('kd_cp') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kd_cp" id="kd_cp" placeholder="Kode" value="<?php echo $kd_cp; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama <?php echo form_error('nm_cp') ?></label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="nm_cp" id="nm_cp" placeholder="Nama" value="<?php echo $nm_cp; ?>" />
                </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan <?php echo form_error('ket_cp') ?></label>
                    <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="ket_cp" id="ket_cp" placeholder="Keterangan"><?php echo $ket_cp; ?></textarea>
                </div>
                </div>
                <input type="hidden" name="id_cp" value="<?php echo $id_cp; ?>" /> 
                 <div class="box-footer text-center">
                <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                <a href="<?php echo site_url('ms_cara_pembayaran') ?>" class="btn btn-default">Cancel</a>
            </div>
            </form>
        </div>

    </div>
</section>
</div>
<div class="content-wrapper">

    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><a><?php echo $title ?></a> Type Marketing</h3>
            </div>
            
            <div class="box-body box-info">

                <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
                   <div class="form-group">
                    <label class="col-sm-2 control-label">Kode Type Marketing <?php echo form_error('kd_type_mr') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="kd_type_mr" id="kd_type_mr" placeholder="Kode Type Marketing" value="<?php echo $kd_type_mr; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Nama Type Marketing <?php echo form_error('nm_type_mr') ?></label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nm_type_mr" id="nm_type_mr" placeholder="Nama Type Marketing" value="<?php echo $nm_type_mr; ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan Type Marketing <?php echo form_error('ket_type_mr') ?></label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="3" name="ket_type_mr" id="ket_type_mr" placeholder="Keterangan Type Marketing"><?php echo $ket_type_mr; ?></textarea>
                    </div>
                </div>

                <div class="box-footer text-center">
                    <input type="hidden" name="id_type_mr" value="<?php echo $id_type_mr; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('ms_type_marketing') ?>" class="btn btn-default">Cancel</a>
                </div>
            </form>

        </div>
    </div>
</section>
</div>